import java.util.Random;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingDeque;

public class Main {
    private static final Random random = new Random();
    private static final int itemsNumber = 50;
    private static final int capacity = 10;

    static class Operation {
        private final BlockingQueue<Integer> fromIvanovToPetrov;
        private final BlockingQueue<Integer> fromPetrovToNechyporchuk;

        public Operation(Integer cap) {
            this.fromIvanovToPetrov = new LinkedBlockingDeque<>(cap);
            this.fromPetrovToNechyporchuk = new LinkedBlockingDeque<>(cap);
        }

        public void produce() throws InterruptedException {
            int items = random.nextInt(4) + 1;
            int produced = 0;
            while (produced != itemsNumber) {
                System.out.println("Іванов виніс " + items + " одиниці майна");
                this.fromIvanovToPetrov.put(items);

                produced += items;
                items = random.nextInt(4) + 1;
                if (items > itemsNumber - produced) {
                    items = itemsNumber - produced;
                }

                try {
                    Thread.sleep(random.nextInt(500) + 100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }

        public void consume() throws InterruptedException {
            int takenInGeneral = 0;
            while (takenInGeneral != itemsNumber) {
                int taken = this.fromIvanovToPetrov.take();
                System.out.println("Петров переніс " + taken + " одиниці майна у вантажівку");
                this.fromPetrovToNechyporchuk.put(taken);

                takenInGeneral += taken;

                try {
                    Thread.sleep(random.nextInt(500) + 100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }

        public void count() throws InterruptedException {
            int counted = 0;
            while (counted != itemsNumber) {
                counted += this.fromPetrovToNechyporchuk.take();
                System.out.println("Нечипорчук вже нарахував: " + counted);

                try {
                    Thread.sleep(random.nextInt(500) + 100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static void main(String[] args) {
        Operation operation = new Operation(capacity);

        Thread Ivanov = new Thread(() -> {
            try {
                operation.produce();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });

        Thread Petrov = new Thread(() -> {
            try {
                operation.consume();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });

        Thread Nechyporchuk = new Thread(() -> {
            try {
                operation.count();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });

        Ivanov.start();
        Petrov.start();
        Nechyporchuk.start();

        try {
            Ivanov.join();
            Petrov.join();
            Nechyporchuk.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}