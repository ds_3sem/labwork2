package main

import (
	"fmt"
	"math/rand"
	"sync"
	"time"
)

func randEnergy(size int) []int {
	energy := make([]int, size)
	seed := rand.NewSource(time.Now().Unix())
	random := rand.New(seed)

	print("Ченці: ")

	for i := 0; i < size; i++ {
		value := random.Intn(150) + 1
		energy[i] = value
		fmt.Printf("%d ", value)
	}

	println("\n")
	return energy
}

func findWinner(energies []int) int {
	if len(energies) <= 1 {
		return energies[0]
	}

	mid := len(energies) / 2

	var wg sync.WaitGroup
	var leftWinner, rightWinner int

	wg.Add(2)

	go func() {
		leftWinner = findWinner(energies[:mid])
		wg.Done()
	}()

	go func() {
		rightWinner = findWinner(energies[mid:])
		wg.Done()
	}()

	wg.Wait()

	if leftWinner > rightWinner {
		fmt.Printf("У змаганні ченців (%d vs %d) перемагає %d\n", leftWinner, rightWinner, leftWinner)
		return leftWinner
	}

	fmt.Printf("У змаганні ченців (%d vs %d) перемагає %d\n", leftWinner, rightWinner, rightWinner)
	return rightWinner
}

func main() {
	energies := randEnergy(20)

	winner := findWinner(energies)

	fmt.Printf("\nПереможець - ченець з енергією Ци %d\n", winner)
}
