import java.util.Random;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

public class Main {
    private final Integer beeGroups;
    private final Integer forestSize;
    private final boolean[] forest;
    private static final AtomicBoolean found = new AtomicBoolean(false);
    private static final AtomicInteger nextForestSection = new AtomicInteger(0);
    private static final Random random = new Random();

    public Main(Integer beeGroups, Integer forestSize) {
        this.beeGroups = beeGroups;
        this.forestSize = forestSize;
        this.forest = createForest();
    }

    private boolean[] createForest() {
        boolean[] forest = new boolean[forestSize];

        int winniePosition = random.nextInt(forestSize);

        forest[winniePosition] = true;
        return forest;
    }

    private class Bees extends Thread {
        private final int groupNum;
        private static int nextGroup = 0;

        public Bees() {
            this.groupNum = nextGroup;
            nextGroup++;
        }

        @Override
        public void run() {
            while (!found.get()) {
                int current = nextForestSection.get();
                if (forest[current]) {
                    found.set(true);
                }
                nextForestSection.set(nextForestSection.incrementAndGet());
                System.out.println("Group " + groupNum + " is searching in section " + current);
                if (forest[current]) {
                    System.out.println("Group " + groupNum + " has found Winnie the Pooh in section " + current);
                }
            }
        }
    }

    private void startProgram() {
        Bees[] bees = new Bees[beeGroups];

        for (int i = 0; i < beeGroups; i++) {
            bees[i] = new Bees();
            bees[i].start();
        }

        for (int i = 0; i < beeGroups; i++) {
            try {
                bees[i].join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        System.out.println("All groups have returned to the hive");
    }

    public static void main(String[] args) {
        Main program = new Main(5, 20);
        program.startProgram();
    }
}